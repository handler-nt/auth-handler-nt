const jwt = require('jsonwebtoken');
const errorHandler = require('error-handler-nt');

let config = null;

/**
 * Add config
 * @param {Object} obj
 */
function configure(obj)
{
  config = obj;
}


/**
 * Check API Key
 * @param {*} req
 * @param {*} res
 * @param {callback} next
 * @return {*}
 */
function checkAPIKey(req, res, next)
{
  if (!config)
    return next(errorHandler.create(500, "No configuration set", "AuthHandler - checkAPIKEY"));

  if (req.query.hasOwnProperty("api_key"))
  {
    if (req.query.api_key === config.API_KEY)
      return next();

    return next(errorHandler.create(401, "API KEY error"));

  }

  return next(errorHandler.create(401, "API KEY not found"));
}

/**
 * Check JsonWebToken
 * @param {Object} req
 * @param {Object} res
 * @param {callback} next
 * @return {*}
 */
function checkJWT(req, res, next)
{
  if (!config)
    return next(errorHandler.create(500, "No configuration set", "AuthHandler - checkJWT"));

  if (req.headers && req.headers.authorization && req.headers.authorization.split(' ')[0] === "Bearer")
  {
    jwt.verify(req.headers.authorization.split(' ')[1], config.JWT_TOKEN, (err, decode) =>
    {
      if (err)
      {
        if (err.name === "TokenExpiredError")
          return errorHandler.createAndSend(res, 401, {message: "Token expired", type: "EXPIRED_TOKEN"});

        return errorHandler.createAndSend(res, 401, {message: "Wrong token", type: "BAD_TOKEN"});
      }

      req['user'] = decode.data;


      return next();
    })
  }
  else
    return errorHandler.createAndSend(res, 401, {message: "No token found", type: "NO_TOKEN"});
}

/**
 * Create JWT using HMAC
 * @param exp
 * @param data
 * @param JWT_TOKEN
 * @return {*}
 */
function createJWT_HMAC(exp, data, JWT_TOKEN = null)
{
  return jwt.sign(
  {
    exp: exp,
    data: data
  },
  (JWT_TOKEN ? JWT_TOKEN : config.JWT_TOKEN));
}


module.exports =
{
  configure,
  checkAPIKey,
  checkJWT,
  createJWT_HMAC
};
