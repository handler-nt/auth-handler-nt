# AuthHandler

Module for authorization verification. 
## Installation

<code>$ yarn install auth-handler-nt --save </code>

## Documentation

- <code>configure(obj)</code><br>
	<b>Set configuration object before using other functions.</b><br>
	Example : 
	```json
	{
		"JWT_TOKEN": "TESTJWT",
		"API_KEY": "SUPERAPIKEY"
	}
	````

	
	
- <code>checkAPIKey(req, res, next)</code><br>
	Middleware for API KEY checking.<br>
	Returns error with status 401 if wrong API KEY.
	
- <code>checkJWT(req, res, next)</code><br>
	Middleware for JWT checking.<br>
	Returns error with status 401 if wrong JWT.
	
- <code>createJWT_HMAC(exp, data, JWT_TOKEN = null)</code><br>
	Return JWT using HMAC algorithm. You can override the config JWT by giving another token in parameter.
	