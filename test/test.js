const assert = require('assert');

const lib = require('../index.js');

describe('Configuration', () =>
{
  it("Should not fail", () =>
  {
    lib.configure({JWT_TOKEN: "TESTJWT", API_KEY: "SUPERAPIKEY"});
  });
});

describe("API KEY", () =>
{
  it("Fail : Verification of API KEY", () =>
  {
    lib
    .checkAPIKey({query: {api_key: "XXX"}}, null, (obj) =>
    {
      assert.notEqual(typeof obj, 'undefined');
    });
  });

  it("Verification of API KEY", () =>
  {
    lib
    .checkAPIKey({query: {api_key: "SUPERAPIKEY"}}, null, (obj) =>
    {
      assert.equal(typeof obj, 'undefined');
    });
  });
});

describe('Check JWT', () =>
{

  it("Fail : Verification of JWT", () =>
  {
    lib
    .checkJWT({headers: {authorization: "Bearer "+"XXX"}}, null, (obj) =>
    {
      assert.notEqual(typeof obj, 'undefined');
    })
  });

  it("Verification of JWT", () =>
  {
    const exp = Math.floor(Date.now() / 1000) + (60 * 60 * 24 * 10);

    let jwt = lib
    .createJWT_HMAC(
    exp,
    {
      id: 1,
      role: "admin",
      entity_id: "x01"
    });

    lib
    .checkJWT({headers: {authorization: "Bearer "+jwt}}, null, (obj) =>
    {
      assert.equal(typeof obj, 'undefined');
    })
  });
});
